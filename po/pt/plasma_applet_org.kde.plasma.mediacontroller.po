msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 01:59+0000\n"
"PO-Revision-Date: 2022-12-12 18:27+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/AlbumArtStackView.qml:190
#, kde-format
msgid "No title"
msgstr "Sem título"

#: package/contents/ui/AlbumArtStackView.qml:190
#: package/contents/ui/main.qml:83
#, kde-format
msgid "No media playing"
msgstr "Nada em reprodução"

#: package/contents/ui/ExpandedRepresentation.qml:406
#: package/contents/ui/ExpandedRepresentation.qml:527
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: package/contents/ui/ExpandedRepresentation.qml:556
#, kde-format
msgid "Shuffle"
msgstr "Baralhar"

#: package/contents/ui/ExpandedRepresentation.qml:582
#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Faixa Anterior"

#: package/contents/ui/ExpandedRepresentation.qml:604
#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Pausa"

#: package/contents/ui/ExpandedRepresentation.qml:604
#: package/contents/ui/main.qml:113
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Tocar"

#: package/contents/ui/ExpandedRepresentation.qml:622
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Faixa Seguinte"

#: package/contents/ui/ExpandedRepresentation.qml:645
#, kde-format
msgid "Repeat Track"
msgstr "Repetir a Faixa"

#: package/contents/ui/ExpandedRepresentation.qml:645
#, kde-format
msgid "Repeat"
msgstr "Repetir"

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Abrir"

#: package/contents/ui/main.qml:129
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Parar"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Sair"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Choose player automatically"
msgstr "Escolher automaticamente o leitor"

#: package/contents/ui/main.qml:307
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"de %1 (%2)\n"
"Botão do meio para pausar\n"
"Deslocar para ajustar o volume"

#: package/contents/ui/main.qml:307
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"%1\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"%1\n"
"Botão do meio para pausar\n"
"Deslocar para ajustar o volume"

#: package/contents/ui/main.qml:321
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"de %1 (em pausa, %2)\n"
"Botão do meio para reproduzir\n"
"Deslocar para ajustar o volume"

#: package/contents/ui/main.qml:321
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"Paused (%1)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"Em pausa (%1)\n"
"Botão do meio para reproduzir\n"
"Deslocar para ajustar o volume"
