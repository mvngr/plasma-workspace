# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Serdar Soytetir <tulliana@gmail.com>, 2008, 2009, 2010, 2012.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2014, 2017, 2021, 2022.
# Kaan Ozdincer <kaanozdincer@gmail.com>, 2014.
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcm_desktopthemedetails\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-03 02:38+0000\n"
"PO-Revision-Date: 2023-08-03 12:34+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#. i18n: ectx: label, entry (name), group (Theme)
#: desktopthemesettings.kcfg:9
#, kde-format
msgid "Name of the current Plasma Style"
msgstr "Geçerli Plasma biçeminin adı"

#: kcm.cpp:111
#, kde-format
msgid "Unable to create a temporary file."
msgstr "Geçici dosya oluşturulamadı."

#: kcm.cpp:122
#, kde-format
msgid "Unable to download the theme: %1"
msgstr "%1 teması indirilemedi"

#: kcm.cpp:147
#, kde-format
msgid "Theme installed successfully."
msgstr "Tema başarıyla kuruldu."

#: kcm.cpp:150 kcm.cpp:156
#, kde-format
msgid "Theme installation failed."
msgstr "Tema kurulumu başarısız."

#: kcm.cpp:262
#, kde-format
msgid "Removing theme failed: %1"
msgstr "%1 temasını kaldırma işlemi başarısız oldu"

#: plasma-apply-desktoptheme.cpp:31
#, kde-format
msgid ""
"This tool allows you to set the theme of the current Plasma session, without "
"accidentally setting it to one that is either not available, or which is "
"already set."
msgstr ""
"Bu araç, geçerli Plasma oturumunuz için temayı, bir öncekinin üzerine "
"yazmadan veya mevcut olmayan birinin yerine ayarlamanızı önleyerek kurmanıza "
"izin verir."

#: plasma-apply-desktoptheme.cpp:35
#, kde-format
msgid ""
"The name of the theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""
"Geçerli Plasma oturumunuz için ayarlamak istediğiniz temanın adı (tam bir "
"yol geçirmek yalnızca yolun son kısmını kullanır)"

#: plasma-apply-desktoptheme.cpp:36
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""
"Sistemde kullanılabilir olan diğer tüm temaları göster (ve geçerli olanı da)"

#: plasma-apply-desktoptheme.cpp:49
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""
"İstenen tema \"%1\", halihazırda geçerli Plasma oturumu için zaten "
"ayarlanmış."

#: plasma-apply-desktoptheme.cpp:65
#, kde-format
msgid "The current Plasma session's theme has been set to %1"
msgstr "Geçerli Plasma oturumunun teması %1 olarak ayarlandı"

#: plasma-apply-desktoptheme.cpp:67
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""
"\"%1\" teması bulunamadı. Tema aşağıdaki seçeneklerden biri olmalıdır: %2"

#: plasma-apply-desktoptheme.cpp:75
#, kde-format
msgid "You have the following Plasma themes on your system:"
msgstr "Sisteminizde aşağıdaki Plasma temaları var:"

#: ui/main.qml:69
#, kde-format
msgid "All Themes"
msgstr "Tüm Temalar"

#: ui/main.qml:70
#, kde-format
msgid "Light Themes"
msgstr "Açık Temalar"

#: ui/main.qml:71
#, kde-format
msgid "Dark Themes"
msgstr "Koyu Temalar"

#: ui/main.qml:72
#, kde-format
msgid "Color scheme compatible"
msgstr "Renk şeması uyumlu"

#: ui/main.qml:97
#, kde-format
msgid "Install from File…"
msgstr "Dosyadan Kur…"

#: ui/main.qml:102
#, kde-format
msgid "Get New…"
msgstr "Yeni Al…"

#: ui/main.qml:115
#, kde-format
msgid "Follows color scheme"
msgstr "Renk şemasını izleyen"

#: ui/main.qml:133
#, kde-format
msgid "Edit Theme…"
msgstr "Temayı Düzenle…"

#: ui/main.qml:140
#, kde-format
msgid "Remove Theme"
msgstr "Temayı Kaldır"

#: ui/main.qml:147
#, kde-format
msgid "Restore Theme"
msgstr "Temayı Geri Yükle"

#: ui/main.qml:189
#, kde-format
msgid "Open Theme"
msgstr "Tema Aç"

#: ui/main.qml:191
#, kde-format
msgid "Theme Files (*.zip *.tar.gz *.tar.bz2)"
msgstr "Tema Dosyaları (*.zip *.tar.gz *.tar.bz2)"

#~ msgid "Get New Plasma Styles…"
#~ msgstr "Yeni Plasma Biçemleri Al…"

#~ msgid "This module lets you choose the Plasma style."
#~ msgstr "Bu modül Plasma biçemini yapılandırmanızı sağlar."
