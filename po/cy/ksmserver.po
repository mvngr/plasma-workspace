# translation of ksmserver.po to Cymraeg
# Copyright (C) 2003 Free Software Foundation, Inc.
# KGyfieithu <kyfieithu@dotmon.com>, 2003.
# KD at KGyfieithu <kyfieithu@dotmon.com>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2003-12-27 12:17+0000\n"
"Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>\n"
"Language-Team: Cymraeg <cy@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:193
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Y trefnydd sesiynau dibynadwy KDE sy'n siarad protocol safonol \n"
"trefnu sesiynau X11R6 (XSMP)."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Adfera'r sesiwn defnyddiwr wedi'i gadw os ar gael"

#: main.cpp:200
#, fuzzy, kde-format
msgid "Also allow remote connections"
msgstr "Caniatáu cysylltiadau pell hefyd."

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:881
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "Trefnydd Sesiynau KDE"

#: server.cpp:886
#, kde-format
msgid "Log Out"
msgstr ""

#: server.cpp:891
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:896
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:902
#, kde-format
msgid "Log Out Without Confirmation"
msgstr ""

#: server.cpp:907
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr ""

#: server.cpp:912
#, kde-format
msgid "Reboot Without Confirmation"
msgstr ""

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Dechreua 'wm' rhag ofn na bod trefnydd ffenestri arall \n"
#~ "yn cymeryd rhan yn y sesiwn. 'kwin' yw'r rhagosodyn."

#, fuzzy
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "&Cau'r cyfrifiadur i lawr"
#~ msgstr[1] "&Cau'r cyfrifiadur i lawr"

#, fuzzy
#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "&Cau'r cyfrifiadur i lawr"
#~ msgstr[1] "&Cau'r cyfrifiadur i lawr"

#, fuzzy
#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "&Cau'r cyfrifiadur i lawr"
#~ msgstr[1] "&Cau'r cyfrifiadur i lawr"

#, fuzzy
#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "&Cau'r cyfrifiadur i lawr"
#~ msgstr[1] "&Cau'r cyfrifiadur i lawr"

#, fuzzy
#~ msgid "Turn Off Computer"
#~ msgstr "&Cau'r cyfrifiadur i lawr"

#, fuzzy
#~ msgid "Restart Computer"
#~ msgstr "&Ail-gychwyn y cyfrifiadur"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Owain Green ar ran KGyfieithu - Meddalwedd Gymraeg"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kyfieithu@dotmon.com"

#~ msgid "Maintainer"
#~ msgstr "Cynhaliwr"

#, fuzzy
#~ msgid "End Session for %1"
#~ msgstr "Diwedd Sesiwn ar gyfer \"%1\""

#, fuzzy
#~ msgid "End Session for %1 (%2)"
#~ msgstr "Diwedd Sesiwn ar gyfer \"%1\""

#, fuzzy
#~ msgid "End Current Session"
#~ msgstr "&Terfynu y sesiwn cyfredol"
