# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-11 02:03+0000\n"
"PO-Revision-Date: 2022-10-17 03:14+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: contents/ui/main.qml:66
#, kde-format
msgid "Switch to activity %1"
msgstr "Skipta í athafnasvæðið \"%1\""

#: contents/ui/main.qml:93
#, kde-format
msgctxt "@action:inmenu"
msgid "&Configure Activities…"
msgstr "Grunnstilla at&hafnasvæði…"
