# translation of plasma_runner_powerdevil.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sangeeta Kumari <sangeeta09@gmail.com>, 2009.
# Rajesh Ranjan <rajesh672@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2010-01-29 16:23+0530\n"
"Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>\n"
"Language-Team: Maithili <maithili.sf.net>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"

#: PowerDevilRunner.cpp:31
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr "सस्पेंड"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr ""

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr ""

#: PowerDevilRunner.cpp:37
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr "हाइबरनेट"

#: PowerDevilRunner.cpp:39
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr ""

#: PowerDevilRunner.cpp:41 PowerDevilRunner.cpp:43 PowerDevilRunner.cpp:64
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr ""

#: PowerDevilRunner.cpp:52
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""

#: PowerDevilRunner.cpp:56
#, kde-format
msgid "Suspends the system to RAM"
msgstr ""

#: PowerDevilRunner.cpp:60
#, kde-format
msgid "Suspends the system to disk"
msgstr ""

#: PowerDevilRunner.cpp:63
#, kde-format
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr ""

#: PowerDevilRunner.cpp:66
#, no-c-format, kde-format
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""

#: PowerDevilRunner.cpp:88
#, kde-format
msgid "Set Brightness to %1%"
msgstr ""

#: PowerDevilRunner.cpp:97
#, kde-format
msgid "Dim screen totally"
msgstr ""

#: PowerDevilRunner.cpp:105
#, kde-format
msgid "Dim screen by half"
msgstr ""

#: PowerDevilRunner.cpp:135
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr ""

#: PowerDevilRunner.cpp:136
#, kde-format
msgid "Suspend to RAM"
msgstr "स्थगित केँ RAM"

#: PowerDevilRunner.cpp:141
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "hibernate"
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr "हाइबरनेट"

#: PowerDevilRunner.cpp:142
#, fuzzy, kde-format
#| msgid "Suspend to Disk"
msgid "Suspend to disk"
msgstr "स्थगित केँ डिस्क"

#: PowerDevilRunner.cpp:210
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr ""

#: PowerDevilRunner.cpp:212
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr ""

#, fuzzy
#~| msgctxt "Note this is a KRunner keyword"
#~| msgid "suspend"
#~ msgid "Suspend"
#~ msgstr "सस्पेंड"
